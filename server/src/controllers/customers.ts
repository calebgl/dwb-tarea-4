import { Request, Response } from "express";
import { Customers, PrismaClient } from "@prisma/client";

const prisma = new PrismaClient();

interface CustomersDTO {
  id: string;
  company: string;
  contact: string | null;
  title: string | null;
  country: string | null;
  city: string | null;
  postalCode: string | null;
  fax: string | null;
}

export const getCustomers = async (
  req: Request,
  res: Response
): Promise<Response> => {
  const customers: Customers[] = await prisma.customers.findMany();
  const customerDTO: CustomersDTO[] = customers.map((customer) => {
    return {
      id: customer.CustomerID,
      company: customer.CompanyName,
      contact: customer.ContactName,
      title: customer.ContactTitle,
      country: customer.Country,
      city: customer.City,
      postalCode: customer.PostalCode,
      fax: customer.Fax,
    };
  });
  return res.status(200).json(customerDTO);
};

export const getCustomer = async (
  req: Request,
  res: Response
): Promise<Response> => {
  const { id } = req.params;

  if (!id)
    return res.status(400).json({ success: false, message: "id is required" });

  const customer: Customers | null = await prisma.customers.findUnique({
    where: { CustomerID: id },
  });

  if (!customer)
    return res
      .status(404)
      .json({ success: false, message: "customer not found" });

  const customerDTO: CustomersDTO = {
    id: customer.CustomerID,
    company: customer.CompanyName,
    contact: customer.ContactName,
    title: customer.ContactTitle,
    country: customer.Country,
    city: customer.City,
    postalCode: customer.PostalCode,
    fax: customer.Fax,
  };

  return res.status(200).json(customerDTO);
};

export const createCustomer = async (
  req: Request,
  res: Response
): Promise<Response> => {
  const {
    id,
    company,
    contact,
    title,
    country,
    city,
    postalCode,
    fax,
  } = req.body;
  const validate: boolean = id && company;

  if (!validate)
    return res
      .status(400)
      .json({ success: false, message: "id and company are required" });

  const customer: Customers | null = await prisma.customers.findUnique({
    where: { CustomerID: id },
  });

  if (customer)
    return res
      .status(406)
      .json({ success: false, message: "id already taken" });

  const newCustomer: Customers = await prisma.customers.create({
    data: {
      CustomerID: id,
      CompanyName: company,
      ContactName: contact,
      ContactTitle: title,
      Country: country,
      City: city,
      PostalCode: postalCode,
      Fax: fax,
    },
  });

  if (!newCustomer)
    return res
      .status(500)
      .json({ success: false, message: "something went wrong" });

  return res
    .status(200)
    .json({ success: true, message: "customer succesfully created" });
};

export const deleteCustomer = async (req: Request, res: Response) => {
  const { id } = req.params;

  if (!id)
    return res.status(400).json({ success: false, message: "id is required" });

  const customer: Customers = await prisma.customers.delete({
    where: { CustomerID: id },
  });

  if (!customer)
    return res
      .status(404)
      .json({ success: false, message: "customer not found" });

  return res
    .status(200)
    .json({ success: true, message: "customer succesfully deleted" });
};
