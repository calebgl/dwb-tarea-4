import { Router } from "express";
import { getProducts, getProduct } from "../controllers/products";

const router: Router = Router();

router.get("/", getProducts);
router.get("/:id", getProduct);
// router.post("/", createEmployee);

export default router;
