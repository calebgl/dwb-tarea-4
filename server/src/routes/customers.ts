import { Router } from "express";
import {
  getCustomers,
  getCustomer,
  createCustomer,
  deleteCustomer,
} from "../controllers/customers";

const router: Router = Router();

router.get("/", getCustomers);
router.get("/:id", getCustomer);
router.post("/", createCustomer);
router.delete("/:id", deleteCustomer);

export default router;
