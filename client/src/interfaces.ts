export interface Employee {
  id: number;
  name: string;
  surname: string | null;
  ocupation: string | null;
  title: string | null;
  country: string | null;
  city: string | null;
}

export interface Customer {
  id: string;
  company: string;
  contact: string | null;
  title: string | null;
  country: string | null;
  city: string | null;
  postalCode: string | null;
  fax: string | null;
}
